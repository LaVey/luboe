import { boot } from 'quasar/wrappers'
import VuePlugin from 'quasar-ui-seper-proj'

export default boot(({ app }) => {
  app.use(VuePlugin)
})
